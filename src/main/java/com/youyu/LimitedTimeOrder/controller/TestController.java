/**
 * @description: TODO
 * @author 86150
 * @date 2023/12/22 12:58
 * @version 1.0
 */

package com.youyu.LimitedTimeOrder.controller;

import com.youyu.LimitedTimeOrder.enmu.OrderStatus;
import com.youyu.LimitedTimeOrder.mapper.OrderMapper;
import com.youyu.LimitedTimeOrder.model.Item;
import com.youyu.LimitedTimeOrder.pojo.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import static com.youyu.LimitedTimeOrder.LimitedTimeOrderApplication.queue;

@RestController
public class TestController {
    @Autowired
    private OrderMapper orderMapper;


    @GetMapping("/test")
    public void test() {
        List<Item<Order>> orders = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Order order = new Order();
            order.setUserId((long) i);
            order.setOrderStatus(OrderStatus.Created);
            order.setOrderTime(LocalDateTime.now());
            order.setProductId(3L);
            order.setProductNum(10);
            orderMapper.insert(order);
            Item<Order> item = new Item<>(ThreadLocalRandom.current().nextLong(1000, 2000) + System.currentTimeMillis(), order);
            queue.add(item);
        }
    }
}
