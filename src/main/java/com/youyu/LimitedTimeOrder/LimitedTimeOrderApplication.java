package com.youyu.LimitedTimeOrder;

import com.youyu.LimitedTimeOrder.enmu.OrderStatus;
import com.youyu.LimitedTimeOrder.model.Item;
import com.youyu.LimitedTimeOrder.pojo.Order;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.concurrent.DelayQueue;

@SpringBootApplication
public class LimitedTimeOrderApplication {

    //    生成全局可以调用的延时队列
    public static DelayQueue<Item<Order>> queue = new DelayQueue<>();

    public static void main(String[] args) {
        SpringApplication.run(LimitedTimeOrderApplication.class, args);
    }

}
