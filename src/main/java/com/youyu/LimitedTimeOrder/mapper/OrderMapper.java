/**
 * @description: TODO
 * @author 86150
 * @date 2023/12/22 10:45
 * @version 1.0
 */

package com.youyu.LimitedTimeOrder.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyu.LimitedTimeOrder.pojo.Order;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OrderMapper extends BaseMapper<Order> {
}
