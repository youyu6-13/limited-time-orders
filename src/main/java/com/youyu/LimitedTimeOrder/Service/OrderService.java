
package com.youyu.LimitedTimeOrder.Service;

import com.youyu.LimitedTimeOrder.enmu.OrderStatus;
import com.youyu.LimitedTimeOrder.mapper.OrderMapper;
import com.youyu.LimitedTimeOrder.model.Item;
import com.youyu.LimitedTimeOrder.pojo.Order;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.Objects;

import static com.youyu.LimitedTimeOrder.LimitedTimeOrderApplication.queue;

@Component
@Slf4j
public class OrderService {
    @Autowired
    private OrderMapper orderMapper;

    private Thread takeOrder;

    // 根据Bean的生命周期,在Bean创建前产生线程

    @PostConstruct
    public void init() {
        takeOrder = new Thread(new OrderTask());
        takeOrder.start();
    }

    // 在Bean结束时，终止线程

    @PreDestroy
    public void close(){takeOrder.interrupt();}

    // delayQueue线程
    private class OrderTask implements Runnable {

        @Override
        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                try {
//                    队列为空，或者不到取出时间阻塞线程
                    Item<Order> orderItem = queue.take();

//                    取出数据，判断订单状态，还未支付标记为超时
                    if (null != orderItem) {
                        Order order = orderItem.getItem();
                        Order order1 = orderMapper.selectById(order.getId());
                        if (!Objects.equals(order1.getOrderStatus(), OrderStatus.Paid)) {
                            order.setOrderStatus(OrderStatus.TimeOut);
                            orderMapper.updateById(order);
                            log.info("{} 订单已超时", order.getId());
                        } else {
                            log.info("{} 订单已支付", order.getId());
                        }
                    }
                } catch (InterruptedException e) {
                    log.info("当前线程被阻塞");
                }
            }
        }
    }
}
