/**
 * @description: TODO
 * @author 86150
 * @date 2023/12/22 12:35
 * @version 1.0
 */

package com.youyu.LimitedTimeOrder.enmu;

public class OrderStatus {
    public final static String Created = "已创建";
    public final static String Paid = "已支付";
    public final static String TimeOut = "已过期";
}
