
package com.youyu.LimitedTimeOrder.model;

import lombok.Data;

import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

@Data
public class Item<T> implements Delayed {
    private Long expirationTime;
    private T item;

    public Item(Long expirationTime, T item) {
        this.expirationTime = expirationTime;
        this.item = item;
    }
    @Override
    public long getDelay(TimeUnit unit) {
        return unit.convert(expirationTime - System.currentTimeMillis(), TimeUnit.MILLISECONDS);
    }

    @Override
    public int compareTo(Delayed o) {
        return (int)(this.getDelay(TimeUnit.MILLISECONDS) - o.getDelay(TimeUnit.MILLISECONDS));
    }


}
