/**
 * @description: TODO
 * @author 86150
 * @date 2023/12/22 10:46
 * @version 1.0
 */

package com.youyu.LimitedTimeOrder.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@TableName("`order`")
public class Order {
    @TableId(type = IdType.AUTO)
    private Long id;
    private Long userId;
    private Long productId;
    private  int productNum;
    private LocalDateTime orderTime;
    private double orderPrice;
    private String orderStatus;
}
